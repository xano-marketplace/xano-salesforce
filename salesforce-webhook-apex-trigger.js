trigger YOUR_TRIGGER_NAME on Lead (after insert) {
	string url = 'YOUR WEBHOOK URL';
    string content = Webhook.jsonContent(Trigger.new, Trigger.old);
    Webhook.callout(url, content);
}
